/*
 * Copyright (c) 2016 Intel Corporation
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include <zephyr/types.h>
#include <zephyr/kernel.h>
#include <zephyr/sys/byteorder.h>
#include <zephyr/drivers/uart.h>
#include <zephyr/usb/usb_device.h>
#include <zephyr/drivers/spi.h>


#include <zephyr/settings/settings.h>

#include <zephyr/device.h>
#include <zephyr/devicetree.h>
#include <zephyr/drivers/gpio.h>
#include <soc.h>

#include <zephyr/bluetooth/bluetooth.h>
#include <zephyr/bluetooth/uuid.h>
#include <zephyr/bluetooth/gatt.h>
#include <zephyr/bluetooth/gap.h>
#include <zephyr/bluetooth/conn.h>
#include <zephyr/bluetooth/hci.h>

#include <zephyr/sys/printk.h>


#include <zephyr/logging/log.h>
#include <stdio.h>
#include "main.h"
#include "ads1299.h"
#include "ble_service.h"

#include <stddef.h>
#include <string.h>
#include <errno.h>

// static struct bt_le_adv_param *adv_param = BT_LE_ADV_PARAM(
// 	(BT_LE_ADV_OPT_CONNECTABLE |
// 	 BT_LE_ADV_OPT_USE_IDENTITY), /* Connectable advertising and use identity address */
// 	800, /* Min Advertising Interval 500ms (800*0.625ms) */
// 	801, /* Max Advertising Interval 500.625ms (801*0.625ms) */
// 	NULL); /* Set to NULL for undirected advertising */
LOG_MODULE_REGISTER(Janitri_Keyar_CM, LOG_LEVEL_INF);


// static K_SEM_DEFINE(ble_init_ok, 0, 1);

struct bt_conn *keyar_conn = NULL;

static struct bt_gatt_exchange_params exchange_params;

/* forward declaration of exchange_func(): */
static void exchange_func(struct bt_conn *conn, uint8_t att_err, struct bt_gatt_exchange_params *params);

// static K_SEM_DEFINE(sensor_data_ready, 0, 1);

#define DEVICE_NAME CONFIG_BT_DEVICE_NAME
#define DEVICE_NAME_LEN (sizeof(DEVICE_NAME) - 1)


#define STACKSIZE 1024
#define PRIORITY 7

#define NOTIFY_INTERVAL 100


#define SPI1NODE DT_NODELABEL(spi1)
static const struct device *spi_dev = DEVICE_DT_GET(SPI1NODE);
/*
#define SPIOP	SPI_WORD_SET(8) | SPI_TRANSFER_MSB
struct spi_dt_spec spispec = SPI_DT_SPEC_GET(DT_NODELABEL(ads1299), SPIOP, 0);*/

#define GPIO0 DT_NODELABEL(gpio0)
const struct device *gpio0_dev = DEVICE_DT_GET(GPIO0);

#define GPIO1 DT_NODELABEL(gpio1)
const struct device *gpio1_dev = DEVICE_DT_GET(GPIO1);

uint8_t app_bytes[6025] = {0};
uint8_t app_ble_bytes[241] = {0};
const struct device *uart= DEVICE_DT_GET(DT_NODELABEL(uart0));

static struct spi_config spi_cfg ={
	.frequency = 250000U,
	.operation = SPI_WORD_SET(8)| SPI_TRANSFER_MSB ,//| SPI_MODE_CPOL,
				 
	.slave = 0,

};

/* STEP 6.2 - Declare the struct of the data item of the FIFOs */
// struct uart_data_t {
// 	void *fifo_reserved;
// 	uint8_t data[241];
// 	uint16_t len;
// };

// /* STEP 6.1 - Declare the FIFOs */
// static K_FIFO_DEFINE(fifo_uart_tx_data);
// static K_FIFO_DEFINE(fifo_uart_rx_data);


static const struct bt_data ad[] = {
	BT_DATA_BYTES(BT_DATA_FLAGS, (BT_LE_AD_GENERAL | BT_LE_AD_NO_BREDR)),
	BT_DATA(BT_DATA_NAME_COMPLETE, DEVICE_NAME, DEVICE_NAME_LEN),
   

};

static const struct bt_data sd[] = {
        BT_DATA_BYTES(BT_DATA_UUID128_ALL, BT_UUID_CUSTOM_SERVICE_VAL),
};



/* Define the thread function  */
void send_data_thread(void)
{
	int offset = 0;

	while (1) {

		/* Send notification, the function sends notifications only if a client is subscribed */
		app_ble_bytes[0] = 0xAA;
		app_ble_bytes[1] = 0xBB;
		for(int i = 0 ; i < 241 ; i++)
		{
			app_ble_bytes[i + 2] = app_bytes[offset + i];

		}
		app_ble_bytes[243] = 0xDD;

		my_spi_send_sensor_notify(app_ble_bytes, sizeof(app_ble_bytes));

		offset = offset + BT_SIZE - 4;

		if(offset == 6025)
		{
			offset = 0;
		}
		k_sleep(K_MSEC(NOTIFY_INTERVAL));
	}
}



/* Define the function to update the connection's PHY */
static void update_phy(struct bt_conn *conn)
{
	int err;
	const struct bt_conn_le_phy_param preferred_phy = {
		.options = BT_CONN_LE_PHY_OPT_NONE,
		.pref_rx_phy = BT_GAP_LE_PHY_2M,
		.pref_tx_phy = BT_GAP_LE_PHY_2M,
	};
	err = bt_conn_le_phy_update(conn, &preferred_phy);
	if (err) {
		LOG_ERR("bt_conn_le_phy_update() returned %d", err);
	}
}

/* Define the function to update the connection's data length */
static void update_data_length(struct bt_conn *conn)
{
	int err;
	struct bt_conn_le_data_len_param my_data_len = {
		.tx_max_len = BT_GAP_DATA_LEN_MAX,
		.tx_max_time = BT_GAP_DATA_TIME_MAX,
	};
	err = bt_conn_le_data_len_update(keyar_conn, &my_data_len);
	if (err) {
		LOG_ERR("data_len_update failed (err %d)", err);
	}
}

/* Define the function to update the connection's MTU */
static void update_mtu(struct bt_conn *conn)
{
	int err;
	exchange_params.func = exchange_func;

	err = bt_gatt_exchange_mtu(conn, &exchange_params);
	if (err) {
		LOG_ERR("bt_gatt_exchange_mtu failed (err %d)", err);
	}
}


static void connected(struct bt_conn *conn, uint8_t err)
{
	if (err) {
		printk("Connection failed (err %u)\n", err);
		return;
	}

	printk("Connected\n");

        keyar_conn = bt_conn_ref(conn);
	
	/* Update the PHY mode */
	update_phy(keyar_conn);
	/* Update the data length and MTU */
	update_data_length(keyar_conn);
	update_mtu(keyar_conn);

	
}

static void disconnected(struct bt_conn *conn, uint8_t reason)
{
	printk("Disconnected (reason %u)\n", reason);

        bt_conn_unref(keyar_conn);

}

/* Add the callback for connection parameter updates */
void on_le_param_updated(struct bt_conn *conn, uint16_t interval, uint16_t latency,
			 uint16_t timeout)
{
	double connection_interval = interval * 1.25; // in ms
	uint16_t supervision_timeout = timeout * 10; // in ms
	LOG_INF("Connection parameters updated: interval %.2f ms, latency %d intervals, timeout %d ms",
		connection_interval, latency, supervision_timeout);
}

/* Write a callback function to inform about updates in the PHY */
void on_le_phy_updated(struct bt_conn *conn, struct bt_conn_le_phy_info *param)
{
	// PHY Updated
	if (param->tx_phy == BT_CONN_LE_TX_POWER_PHY_1M) {
		LOG_INF("PHY updated. New PHY: 1M");
	} else if (param->tx_phy == BT_CONN_LE_TX_POWER_PHY_2M) {
		LOG_INF("PHY updated. New PHY: 2M");
	} else if (param->tx_phy == BT_CONN_LE_TX_POWER_PHY_CODED_S8) {
		LOG_INF("PHY updated. New PHY: Long Range");
	}
}
/* Write a callback function to inform about updates in data length */
void on_le_data_len_updated(struct bt_conn *conn, struct bt_conn_le_data_len_info *info)
{
	uint16_t tx_len = info->tx_max_len;
	uint16_t tx_time = info->tx_max_time;
	uint16_t rx_len = info->rx_max_len;
	uint16_t rx_time = info->rx_max_time;
	LOG_INF("Data length updated. Length %d/%d bytes, time %d/%d us", tx_len, rx_len, tx_time,
		rx_time);
}



/* Implement callback function for MTU exchange */
static void exchange_func(struct bt_conn *conn, uint8_t att_err,
			  struct bt_gatt_exchange_params *params)
{
	LOG_INF("MTU exchange %s", att_err == 0 ? "successful" : "failed");
	if (!att_err) {
		uint16_t payload_mtu = bt_gatt_get_mtu(conn) - 3; // 3 bytes used for Attribute headers.
		LOG_INF("New MTU: %d bytes", payload_mtu);
	}
}

struct bt_conn_cb connection_callbacks = {
	.connected = connected,
	.disconnected = disconnected,
        /* Add the callback for connection parameter updates */
	.le_param_updated = on_le_param_updated,
	/* Add the callback for PHY mode updates */
	.le_phy_updated = on_le_phy_updated,
	/* Add the callback for data length updates */
	.le_data_len_updated = on_le_data_len_updated,

};
/*do{	
		err = spi_write(spi_dev,&spi_cfg, &tx_spi_buf_set);
		if (err < 0) { break;}
		

		err = spi_read(spi_dev, &spi_cfg, &rx_spi_buf_set);
		
	}while(false);	*/
/**/

static void gpio_drdy_change(const struct device *dev,
                         struct gpio_callback *cb,
                         uint32_t pins)
{
    if (pins == BIT(GPIO_DRDY)) {
        intDRDY = true;
		//k_sem_give(&sensor_data_ready);
    }
}

static struct gpio_callback gpio_cb;

uint8_t transferSPI(uint8_t regs)
{
	int err;
	// Set the transmit and receive buffers 
	/* Set the transmit and receive buffers */
	uint8_t tx_buffer[1];
	tx_buffer[0] = regs;// {(regs & 0x7F), 0xFF};

	 uint8_t data[2] = {0};

	struct spi_buf tx_spi_buf[]			= {
		{.buf = tx_buffer, .len = sizeof(tx_buffer)},
	};

	struct spi_buf_set tx_spi_buf_set 	= {.buffers = tx_spi_buf, .count = 1};

	struct spi_buf rx_spi_bufs[] 			= { {.buf = data, .len = sizeof(data)}
	};
	struct spi_buf_set rx_spi_buf_set	= {.buffers = rx_spi_bufs, .count = 1}; 

	err = spi_transceive(spi_dev, &spi_cfg, &tx_spi_buf_set, &rx_spi_buf_set);
	if (err < 0) {
		LOG_ERR("spi_transceive_dt() failed, err: %d", err);
		return err;
	}

	return data[1];
}






void main(void)
{

	gpio_pin_configure(gpio0_dev, GPIO_START, GPIO_OUTPUT);
	gpio_pin_configure(gpio0_dev, GPIO_CS, GPIO_OUTPUT);
	gpio_pin_configure(gpio1_dev, GPIO_CLKSEL, GPIO_OUTPUT);
	gpio_pin_configure(gpio1_dev, GPIO_RESET, GPIO_OUTPUT);
	gpio_pin_configure(gpio1_dev, GPIO_DRDY, GPIO_INPUT);
    int err;
	err = bt_enable(NULL);
	if (err) {
		printk("Bluetooth init failed (err %d)\n", err);
		return;
	}

    bt_conn_cb_register(&connection_callbacks);

	printk("Bluetooth initialized\n");

	if (IS_ENABLED(CONFIG_SETTINGS)) {
		settings_load();
	}


	err = bt_le_adv_start(BT_LE_ADV_CONN, ad, ARRAY_SIZE(ad), sd, ARRAY_SIZE(sd));
	if (err) {
		printk("Advertising failed to start (err %d)\n", err);
		return;
	}

	printk("Advertising successfully started\n");


	
	
	if (!device_is_ready(uart)){
		printk("UART device not ready\r\n");
		return 1 ;
	}
		
		
		
    if(!device_is_ready(spi_dev)){
		printk("device not ready\n");
		return;
	}
	/* Check if SPI and GPIO devices are ready 
	err = spi_is_ready_dt(&spispec);
	if (!err) {
		LOG_ERR("Error: SPI device is not ready, err: %d", err);
		return 0;
	}*/
	printk("SPI is ready to use\n");
	gpio_pin_interrupt_configure(gpio1_dev, GPIO_DRDY, GPIO_INT_EDGE_TO_ACTIVE);
	gpio_init_callback(&gpio_cb, gpio_drdy_change, BIT(GPIO_DRDY));
    gpio_add_callback(gpio1_dev, &gpio_cb);



	gpio_pin_set(gpio1_dev, GPIO_CLKSEL, 1);
	gpio_pin_set(gpio1_dev, GPIO_RESET, 1);
	gpio_pin_set(gpio0_dev, GPIO_CS, 0);
	gpio_pin_set(gpio0_dev, GPIO_START, 1);

	

	ADS_Init();
	
	
	while(1){

		//k_sem_take(&sensor_data_ready, K_FOREVER);

		if(intDRDY)
		{
			intDRDY = false;
			ADS_RDATA();

		}
	}

}
/* Define and initialize a thread to send data periodically */
K_THREAD_DEFINE(send_data_thread_id, STACKSIZE, send_data_thread, NULL, NULL, NULL, PRIORITY, 0, 0);


















/*uint8_t tx_buffer = regs;
	struct spi_buf tx_spi_buf		= {.buf = (void *)&tx_buffer, .len = 1};
	struct spi_buf_set tx_spi_buf_set 	= {.buffers = &tx_spi_buf, .count = 1};
	struct spi_buf rx_spi_bufs 		= {.buf = data, .len = sizeof(data)};
	struct spi_buf_set rx_spi_buf_set	= {.buffers = &rx_spi_bufs, .count = 1};
*/



/*
#define LED0_NODE DT_ALIAS(led0)


static const struct gpio_dt_spec led = GPIO_DT_SPEC_GET(LED0_NODE, gpios);


	int ret;

	if (!gpio_is_ready_dt(&led)) {
		return 0;
	}

	ret = gpio_pin_configure_dt(&led, GPIO_OUTPUT_ACTIVE);
	if (ret < 0) {
		return 0;
	}

	while (1) {
		ret = gpio_pin_toggle_dt(&led);
		if (ret < 0) {
			return 0;
		}
		k_msleep(SLEEP_TIME_MS);
	}
	return 0;
}
*/