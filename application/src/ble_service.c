#include <zephyr/types.h>
#include <zephyr/kernel.h>
#include <zephyr/sys/byteorder.h>



#include <zephyr/settings/settings.h>

#include <zephyr/device.h>
#include <zephyr/devicetree.h>
#include <zephyr/drivers/gpio.h>
#include <soc.h>

#include <zephyr/bluetooth/bluetooth.h>
#include <zephyr/bluetooth/uuid.h>
#include <zephyr/bluetooth/gatt.h>
#include <zephyr/bluetooth/gap.h>
#include <zephyr/bluetooth/conn.h>
#include <zephyr/bluetooth/hci.h>

#include <zephyr/sys/printk.h>


#include <zephyr/logging/log.h>
#include <stdio.h>
#include "main.h"
#include "ads1299.h"
#include "ble_service.h"

#include <stddef.h>
#include <string.h>
#include <errno.h>



static bool notify_mysensor_enabled;

static bool indicate_enabled;

// static struct bt_nus_cb spi_cb;
// static void spi_ccc_cfg_changed(const struct bt_gatt_attr *attr,
// 				  uint16_t value)
// {
// 	if (spi_cb.send_enabled) {
// 		spi_cb.send_enabled(value == BT_GATT_CCC_NOTIFY ?
// 			BT_NUS_SEND_STATUS_ENABLED : BT_NUS_SEND_STATUS_DISABLED);
// 	}
// }

// static ssize_t on_receive(struct bt_conn *conn,
// 			  const struct bt_gatt_attr *attr,
// 			  const void *buf,
// 			  uint16_t len,
// 			  uint16_t offset,
// 			  uint8_t flags)
// {
// 	LOG_DBG("Received data, handle %d, conn %p",
// 		attr->handle, (void *)conn);

// 	if (spi_cb.received) {
// 		spi_cb.received(conn, buf, len);
// }
// 	return len;
// }

// static void on_sent(struct bt_conn *conn, void *user_data)
// {
// 	ARG_UNUSED(user_data);

// 	LOG_DBG("Data send, conn %p", (void *)conn);

// 	if (spi_cb.sent) {
// 		spi_cb.sent(conn);
// 	}
// }
// /*call back functions of tx and rx*/
static void myspible_ccc_cfg_changed(const struct bt_gatt_attr *attr, uint16_t value)
{
    printk("CCC configuration changed. New value: 0x%04X\n", value);

    notify_mysensor_enabled = (value == BT_GATT_CCC_NOTIFY);

   indicate_enabled = (value == BT_GATT_CCC_INDICATE);

    // /* Check if notifications/indications are enabled or disabled based on the value*/
    // if (value == BT_GATT_CCC_INDICATE) {
    //     printk("Indications enabled.\n");
    //     // Perform actions when indications are enabled, if necessary
    // } else if (value == BT_GATT_CCC_NOTIFY) {
    //     printk("Notifications enabled.\n");
    //     notify_mysensor_enabled = (value == BT_GATT_CCC_NOTIFY);
    //     /* Perform actions when notifications are enabled, if necessary*/
    // } else {
    //     printk("Notifications/Indications disabled.\n");
    //     /* Perform actions when notifications/indications are disabled, if necessary*/
    // }
}

/* Create and add the CUSTOM service to the Bluetooth LE stack */
BT_GATT_SERVICE_DEFINE(my_cus_svc, BT_GATT_PRIMARY_SERVICE(BT_UUID_CUSTOM_SERVICE),
		       /* Create and add the TX characteristic */
		       BT_GATT_CHARACTERISTIC(BT_UUID_CUS_RX, BT_GATT_CHRC_NOTIFY | BT_GATT_CHRC_INDICATE,
					       BT_GATT_PERM_READ,NULL, NULL, NULL),
                       /*  Create and add the Client Characteristic Configuration Descriptor */
	                BT_GATT_CCC(NULL, BT_GATT_PERM_READ | BT_GATT_PERM_WRITE),

		       /* Create and add the RX characteristic. */
		       BT_GATT_CHARACTERISTIC(BT_UUID_CUS_TX , BT_GATT_CHRC_WRITE | BT_GATT_CHRC_WRITE_WITHOUT_RESP,
					      BT_GATT_PERM_WRITE, NULL,NULL, NULL),

);

// int bt_spi_init(struct bt_spi_cb *callbacks)
// {
// 	if (callbacks) {
// 		spi_cb.received = callbacks->received;
// 		spi_cb.sent = callbacks->sent;
// 		spi_cb.send_enabled = callbacks->send_enabled;
// 	}

// 	return 0;
// }

// int bt_spi_send(struct bt_conn *conn, const uint8_t *data, uint16_t len)
// {
// 	struct bt_gatt_notify_params params = {0};
// 	const struct bt_gatt_attr *attr = &cus_svc.attrs[2];

// 	params.attr = attr;
// 	params.data = data;
// 	params.len = len;
// 	params.func = on_sent;

// 	if (!conn) {
// 		LOG_DBG("Notification send to all connected peers");
// 		return bt_gatt_notify_cb(NULL, &params);
// 	} else if (bt_gatt_is_subscribed(conn, attr, BT_GATT_CCC_NOTIFY)) {
// 		return bt_gatt_notify_cb(conn, &params);
// 	} else {
// 		return -EINVAL;
// 	}
// }

int my_spi_send_sensor_notify(uint8_t sensor_value[], uint8_t size)
{
	if (!notify_mysensor_enabled) {
		return -EACCES;
	}

	return bt_gatt_notify(NULL, &my_cus_svc.attrs[1], sensor_value, size);
}

