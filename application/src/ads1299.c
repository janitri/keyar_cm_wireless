#include <zephyr/types.h>
#include <zephyr/kernel.h>
#include <zephyr/sys/byteorder.h>
#include <zephyr/drivers/uart.h>
#include <zephyr/usb/usb_device.h>
#include <zephyr/drivers/spi.h>

#include <zephyr/settings/settings.h>

#include <zephyr/device.h>
#include <zephyr/devicetree.h>
#include <zephyr/drivers/gpio.h>
#include <soc.h>


#include <zephyr/bluetooth/bluetooth.h>
#include <zephyr/bluetooth/uuid.h>
#include <zephyr/bluetooth/gatt.h>
#include <zephyr/bluetooth/gap.h>
#include <zephyr/bluetooth/conn.h>
#include <zephyr/bluetooth/hci.h>

#include <zephyr/sys/printk.h>

#include <zephyr/logging/log.h>
#include <stdio.h>
#include "main.h"
#include "ads1299.h"
#include "ble_service.h"



bool intDRDY = false; // Flag to ready data from ADS1298

bool isDaisy = false;		// does this have a daisy chain board?

int stat_1, stat_2;    	// used to hold the status register for boards 1 and 2

uint8_t regData[24];		// array with data from all registers
int32_t channelData [16];	// array used when reading channel data board 1+2

uint16_t ByteFlag=14; //Byte Flag for transmission frame
uint8_t just_once=0; //To include Header only once
uint8_t UART_Data[6025]={0};// Transmission Buffer
uint8_t inByte,inByte1,inByte2,inByte3;
uint8_t timee[3]={0};
int8_t Nstat= 0; //Lead-Off Status N
int8_t Pstat= 0; //Lead-Off Status P


void ADS_Init(){

		ADS_RESET();
		k_msleep(500);

		ADS_SDATAC();
		k_msleep(500);

		ADS_getDeviceID();
		k_msleep(1000);

		
		ADS_WREG(CONFIG1,0xD5); //500 LP
		k_msleep(100);
		ADS_WREG(CONFIG2,0xD3); //
		k_msleep(100);
		ADS_WREG(CONFIG3,0xEC); //
		k_msleep(100);

		

		//Gain set
		ADS_WREG(CH1SET,0x00);
		k_msleep(10);
		ADS_WREG(CH2SET,0x00);
		k_msleep(10);
		ADS_WREG(CH3SET,0x00);
		k_msleep(10);
		ADS_WREG(CH4SET,0x00);
		k_msleep(10);

		//RLD
		ADS_WREG(RLD_SENSP,0x0E);
		k_msleep(10);
		ADS_WREG(RLD_SENSN,0x0E);
		k_msleep(10);

		
		ADS_WREG(LOFF_SENSP,0x00);
		k_msleep(10);
		ADS_WREG(LOFF_SENSN,0x00);
		k_msleep(10);
		
		ADS_WREG(CONFIG4,0x00);
		k_msleep(10);
		

		ADS_START();
		k_msleep(10);

		ADS_RDATAC();            // enter Read Data Continuous mode
		k_msleep(10);



}

void ADS_SDATAC(){

	gpio_pin_set(gpio0_dev, GPIO_CS, 0);	
	
	transferSPI(_SDATAC);

	gpio_pin_set(gpio0_dev, GPIO_CS, 1);
	
}

//start data conversion 
void ADS_START() {	

	gpio_pin_set(gpio0_dev, GPIO_CS, 0);
	transferSPI(_START);
	gpio_pin_set(gpio0_dev, GPIO_CS, 1);
}
	

//stop data conversion 
void ADS_STOP() {			

	gpio_pin_set(gpio0_dev, GPIO_CS, 0);	
	transferSPI(_STOP);
	gpio_pin_set(gpio0_dev, GPIO_CS, 1);
}

void ADS_RESET(){
	gpio_pin_set(gpio0_dev, GPIO_CS, 0);
	transferSPI(_RESET);
	gpio_pin_set(gpio0_dev, GPIO_CS, 1);
	
}

void ADS_RDATAC() {
	gpio_pin_set(gpio0_dev, GPIO_CS, 0);
	transferSPI(_RDATAC);
	gpio_pin_set(gpio0_dev, GPIO_CS, 1);
}

void ADS_WAKEUP() {
	gpio_pin_set(gpio0_dev, GPIO_CS, 0);
	transferSPI( _WAKEUP);
	gpio_pin_set(gpio0_dev, GPIO_CS, 1);
}

// Register Read/Write Commands
uint8_t ADS_getDeviceID() {			// simple hello world com check

	uint8_t data = ADS_RREG(0);
	printk("ID : 0x%02X\n", data);
	return data;
	
}

uint8_t ADS_RREG(uint8_t _address){		//  reads ONE register at _address
	uint8_t opcode1 = _address + 0x20; 		//  RREG expects 001rrrrr where rrrrr = _address

	gpio_pin_set(gpio0_dev, GPIO_CS, 0);		//  open SPI

	transferSPI( opcode1); 								//  opcode1
	transferSPI( 0x01); 											//  opcode2

	regData[_address] = transferSPI( 0);		//  update mirror location with returned byte

	gpio_pin_set(gpio0_dev, GPIO_CS, 1);		//  close SPI

	//HAL_UART_Transmit(&huart1, &regData[0], sizeof(uint8_t),0x1000);

	return regData[_address];			// return requested register value
}

// Read more than one register starting at _address
void ADS_RREGS(uint8_t _address, uint8_t _numRegistersMinusOne) {
	//	for(byte i = 0; i < 0x17; i++){
	//		regData[i] = 0;									//  reset the regData array
	//	}
	int i;

	uint8_t opcode1 = _address + 0x20; 				//  RREG expects 001rrrrr where rrrrr = _address

	gpio_pin_set(gpio0_dev, GPIO_CS, 0);//  open SPI

	transferSPI( opcode1); 										//  opcode1
	transferSPI( _numRegistersMinusOne);				//  opcode2

	for(i = 0; i <= _numRegistersMinusOne; i++){
		regData[_address + i] = transferSPI( 0x00); 	//  add register byte to mirror array
	}

	gpio_pin_set(gpio0_dev, GPIO_CS, 1);		//  close SPI

}


void ADS_WREG(uint8_t _address, uint8_t _value) {	//  Write ONE register at _address
	uint8_t opcode1 = _address + 0x40; 				//  WREG expects 010rrrrr where rrrrr = _address

	gpio_pin_set(gpio0_dev, GPIO_CS, 0);					//  open SPI

	transferSPI( opcode1);											//  Send WREG command & address
	transferSPI( 0x00);												//	Send number of registers to read -1
	transferSPI( _value);											//  Write the value to the register

	gpio_pin_set(gpio0_dev, GPIO_CS, 1);						//  close SPI

	regData[_address] = _value;			//  update the mirror array

}

void ADS_WREGS(uint8_t _address, uint8_t _numRegistersMinusOne){
	int i;

	uint8_t opcode1 = _address + 0x40;				//  WREG expects 010rrrrr where rrrrr = _address

	gpio_pin_set(gpio0_dev, GPIO_CS, 0);					//  open SPI

	transferSPI( opcode1);											//  Send WREG command & address
	transferSPI( _numRegistersMinusOne);				//	Send number of registers to read -1	

	for (i=_address; i <=(_address + _numRegistersMinusOne); i++){
		transferSPI( regData[i]);								//  Write to the registers
	}	

	gpio_pin_set(gpio0_dev, GPIO_CS, 1); 						//  close SPI

}

void ADS_updateChannelData(){
	uint8_t inByte;
	int i,j;				// iterator in loop
	int nchan=8;  //assume 8 channel.  If needed, it automatically changes to 16 automatically in a later block.


	for(i = 0; i < nchan; i++){
		channelData[i] = 0;
	}

	gpio_pin_set(gpio0_dev, GPIO_CS, 0);				//  open SPI

	// READ CHANNEL DATA FROM FIRST ADS IN DAISY LINE
	for(i = 0; i < 3; i++){										//  read 3 byte status register from ADS 1 (1100+LOFF_STATP+LOFF_STATN+GPIO[7:4])
		inByte = transferSPI( 0x00);
		stat_1 = (stat_1<<8) | inByte;				
	}

	for(i = 0; i < 8; i++){
		for( j=0; j<3; j++){		//  read 24 bits of channel data from 1st ADS in 8 3 byte chunks
			inByte = transferSPI( 0x00);
			channelData[i] = (channelData[i]<<8) | inByte;
		}
	}

	if (isDaisy) {
		nchan = 16;
		// READ CHANNEL DATA FROM SECOND ADS IN DAISY LINE
		for(i = 0; i < 3; i++){			//  read 3 byte status register from ADS 2 (1100+LOFF_STATP+LOFF_STATN+GPIO[7:4])
			inByte = transferSPI( 0x00);
			stat_2 = (stat_1<<8) | inByte;				
		}

		for( i = 8; i < 16; i++){
			for( j = 0; j < 3; j++){		//  read 24 bits of channel data from 2nd ADS in 8 3 byte chunks
				inByte = transferSPI( 0x00);
				channelData[i] = (channelData[i]<<8) | inByte;
			}
		}
	}

	gpio_pin_set(gpio0_dev, GPIO_CS, 1);		// CLOSE SPI

	
}

//read data
void ADS_RDATA() {				//  use in Stop Read Continuous mode when DRDY goes low
	

	
	//	uint8_t UART_Data[6004];
	int i,j;
	int nchan = 4;	//assume 4 channel.  If needed, it automatically changes to 16 automatically in a later block.

	//	memset(UART_Data,0, sizeof(UART_Data));
	stat_1 = 0;							//  clear the status registers
	stat_2 = 0;	

	for(i = 0; i < nchan; i++){
		channelData[i] = 0;
	}

	if(just_once==0)
	{
		memset(UART_Data,0,sizeof(UART_Data));
		just_once=1;
		//counter++;
		UART_Data[0] = 0x40; // Start Byte(@)
		UART_Data[1] = 0x07; // Year(2020)
		UART_Data[2] = 0xE4; // Year
		UART_Data[3] = 0x0C; // Month(12)
		UART_Data[4] = 0x00; // Device ID(10)
		UART_Data[5] = 0x04; // Device ID(10)
		UART_Data[6] = 0xDC; // Product Version(220)
		UART_Data[7] = 0xBD; // Firmware Version(189)
		UART_Data[8] = 0x00; // Signal Strength(0)
		UART_Data[9] = 0x00; // Error Byte 1(0)
		UART_Data[10] = 0x00; // Error Byte 2(0)
		UART_Data[11] = 0x00; // Battery(00)
		UART_Data[12] = 0x00; // Time(1)
		UART_Data[13] = 0x00; // Time(2)
		UART_Data[14] = 0x00; // Time(3)

		UART_Data[6015] = 0x24; // End Byte($)
		UART_Data[6016] = 0xFF;
		UART_Data[6017] = 0xDD;
		UART_Data[6018] = 0x42;
		UART_Data[6019] = 0x59;
		UART_Data[6020] = 0x24; 
		UART_Data[6021] = 0x25;
		UART_Data[6022] = 0x26;
		UART_Data[6023] = 0x25;
		UART_Data[6024] = ID;// End Byte($)

		//		UART_Data[6012] = 0x24; // End Byte($)
	}


		gpio_pin_set(gpio0_dev, GPIO_CS, 0);				//  CLOSE SPI
	//	transferSPI( _RDATA);
	
	transferSPI( _RDATAC);


	// READ CHANNEL DATA FROM FIRST ADS IN DAISY LINE
	for(i = 0; i < 3; i++){			//  read 3 byte status register (1100+LOFF_STATP+LOFF_STATN+GPIO[7:4])
		inByte = transferSPI( 0x00);
		stat_1 = (stat_1<<8) | inByte;				
	}

	

	

	//CH1
	inByte1 = transferSPI(0x00);
	inByte2 = transferSPI(0x00);
	inByte3 = transferSPI(0x00);

	UART_Data[++ByteFlag] = inByte1;
	UART_Data[++ByteFlag] = inByte2;
	UART_Data[++ByteFlag] = inByte3;


	//CH2
	inByte1 = transferSPI(0x00);
	inByte2 = transferSPI(0x00);
	inByte3 = transferSPI(0x00);
	
	UART_Data[++ByteFlag] = inByte1;
	UART_Data[++ByteFlag] = inByte2;
	UART_Data[++ByteFlag] = inByte3;

	//CH3
	inByte1 = transferSPI(0x00);
	inByte2 = transferSPI(0x00);
	inByte3 = transferSPI(0x00);

	UART_Data[++ByteFlag] = inByte1;
	UART_Data[++ByteFlag] = inByte2;
	UART_Data[++ByteFlag] = inByte3;


	//CH4
	inByte1 = transferSPI(0x00);
	inByte2 = transferSPI(0x00);
	inByte3 = transferSPI(0x00);

	UART_Data[++ByteFlag] = inByte1;
	UART_Data[++ByteFlag] = inByte2;
	UART_Data[++ByteFlag] = inByte3;




	


	

	if(ByteFlag==6014)
	{
		for(int i = 0 ;i < 6025 ; i++)
		{
			app_bytes[i] = UART_Data[i];
		}	
		ByteFlag=14;
		just_once=0;
	}
	gpio_pin_set(gpio0_dev, GPIO_CS, 1);				//  open SPI

}


