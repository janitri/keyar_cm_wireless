/*
 * Copyright (c) 2018 Nordic Semiconductor ASA
 *
 * SPDX-License-Identifier: LicenseRef-Nordic-5-Clause
 */

#ifndef BT_LBS_H_
#define BT_LBS_H_

/**@file
 * @defgroup bt_lbs LED Button Service API
 * @{
 * @brief API for the LED Button Service (LBS).
 */

#ifdef __cplusplus
extern "C" {
#endif

#include <zephyr/types.h>

/** @brief LBS Service UUID. */
#define BT_UUID_CUSTOM_SERVICE_VAL \
	BT_UUID_128_ENCODE(0x18692343, 0xe512, 0x4595, 0x8f06, 0xc10b21430f29)

#define BT_UUID_CUS_TX_VAL                                                                     \
	BT_UUID_128_ENCODE(0x18692344, 0xe512, 0x4595, 0x8f06, 0xc10b21430f30)

#define BT_UUID_CUS_RX_VAL                                                                     \
	BT_UUID_128_ENCODE(0x18692345, 0xe512, 0x4595, 0x8f06, 0xc10b21430f31)

#define BT_UUID_CUSTOM_SERVICE      BT_UUID_DECLARE_128(BT_UUID_CUSTOM_SERVICE_VAL)
#define BT_UUID_CUS_TX              BT_UUID_DECLARE_128(BT_UUID_CUS_TX_VAL)
#define BT_UUID_CUS_RX              BT_UUID_DECLARE_128(BT_UUID_CUS_RX_VAL)

//int my_spi_send_sensor_notify(uint8_t sensor_value[], uint8_t size);
/** @brief Pointers to the callback functions for service events. */
// struct bt_spi_cb {
// 	/** @brief Data received callback.
// 	 *
// 	 * The data has been received as a write request on the NUS RX
// 	 * Characteristic.
// 	 *
// 	 * @param[in] conn  Pointer to connection object that has received data.
// 	 * @param[in] data  Received data.
// 	 * @param[in] len   Length of received data.
// 	 */
// 	void (*received)(struct bt_conn *conn,
// 			 const uint8_t *const data, uint16_t len);

// 	/** @brief Data sent callback.
// 	 *
// 	 * The data has been sent as a notification and written on the NUS TX
// 	 * Characteristic.
// 	 *
// 	 * @param[in] conn Pointer to connection object, or NULL if sent to all
// 	 *                 connected peers.
// 	 */
// 	void (*sent)(struct bt_conn *conn);

// 	/** @brief Send state callback.
// 	 *
// 	 * Indicate the CCCD descriptor status of the NUS TX characteristic.
// 	 *
// 	 * @param[in] status Send notification status.
// 	 */
// 	void (*send_enabled)(enum bt_spi_send_status status);

// };

// /**@brief Initialize the service.
//  *
//  * @details This function registers a GATT service with two characteristics,
//  *          TX and RX. A remote device that is connected to this service
//  *          can send data to the RX Characteristic. When the remote enables
//  *          notifications, it is notified when data is sent to the TX
//  *          Characteristic.
//  *
//  * @param[in] callbacks  Struct with function pointers to callbacks for service
//  *                       events. If no callbacks are needed, this parameter can
//  *                       be NULL.
//  *
//  * @retval 0 If initialization is successful.
//  *           Otherwise, a negative value is returned.
//  */
// int bt_spi_init(struct bt_spi_cb *callbacks);

// /**@brief Send data.
//  *
//  * @details This function sends data to a connected peer, or all connected
//  *          peers.
//  *
//  * @param[in] conn Pointer to connection object, or NULL to send to all
//  *                 connected peers.
//  * @param[in] data Pointer to a data buffer.
//  * @param[in] len  Length of the data in the buffer.
//  *
//  * @retval 0 If the data is sent.
//  *           Otherwise, a negative value is returned.
//  */
// int bt_spi_send(struct bt_conn *conn, const uint8_t *data, uint16_t len);

// /**@brief Get maximum data length that can be used for @ref bt_nus_send.
//  *
//  * @param[in] conn Pointer to connection Object.
//  *
//  * @return Maximum data length.
//  */
// static inline uint32_t bt_spi_get_mtu(struct bt_conn *conn)
// {
// 	/* According to 3.4.7.1 Handle Value Notification off the ATT protocol.
// 	 * Maximum supported notification is ATT_MTU - 3 */
// 	return bt_gatt_get_mtu(conn) - 3;
// }


#ifdef __cplusplus
}
#endif

/**
 * @}
 */

#endif /* BT_LBS_H_ */
